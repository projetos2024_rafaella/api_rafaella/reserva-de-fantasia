const e = require("express");

module.exports = class usuarioController {
    static async postUsuario(req, res) {
        const { email, telefone, senha, confirmarSenha } = req.body;

        try {
            if (email === '' || telefone === '' || senha === '' || confirmarSenha === '') {
                throw new Error("Por favor, preencha todos os campos corretamente");
            }
            else {
                return res.status(200).json({ message: "Usuário criado com sucesso." });
            }
            // Lógica para criar um novo usuário no banco de dados
        } catch (error) {
            console.error("Erro ao criar usuário:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    static async updateUsuario(req, res) {
        const { email, telefone, senha, confirmarSenha } = req.body;

        try {
            if (!email || !telefone || !senha || !confirmarSenha) {
                throw new Error("Preencha todos os campos corretamente");
            }

            // Lógica para atualizar o usuário no banco de dados

            return res.status(200).json({ message: "Usuário atualizado" });
        } catch (error) {
            console.error("Erro ao atualizar usuário:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    static async getUsuario(req, res) {
        try {
            // Lógica para obter informações do usuário do banco de dados
            const email = "email@example.com";
            const telefone = "16 993456732";
            const senha = "yasmim123";
            const confirmarSenha = "yasmim123";

            return res.status(200).json({ email, telefone, senha, confirmarSenha });
        } catch (error) {
            console.error("Erro ao obter informações do usuário:", error.message);
            return res.status(500).json({ message: "Erro interno do servidor" });
        }
    }

    static async deleteUsuarioById(req, res) {
        const { id } = req.params;

        try {
            if (!id || isNaN(id) || parseInt(id) <= 0) {
                throw new Error("ID de usuário inválido!");
            }

            // Lógica para excluir o usuário do banco de dados

            return res.status(200).json({ message: "Usuário removido com ID " + id });
        } catch (error) {
            console.error("Erro ao excluir usuário:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    static async Login(req, res) {
        const { email, senha } = req.body; // Extrai os campos 'email' e 'senha' do corpo da requisição
        try {
            console.log("Credenciais: ", email, senha); // Registra no console as credenciais recebidas
            if (!email || !senha) { // Verifica se o email ou senha não foram fornecidos
                return res.status(400).json({ message: "Por favor, preencha todos os campos corretamente" }); // Retorna uma resposta de erro se algum campo estiver vazio
            }
    
            // Lógica para autenticar o usuário no banco de dados
            if (email === "email@example.com" && senha === "yasmim123") { // Verifica se as credenciais correspondem a um usuário válido
                console.log("Credenciais válidas. Login bem-sucedido."); // Registra no console que as credenciais são válidas
                return res.status(200).json({ message: "Login bem-sucedido." }); // Retorna uma resposta de sucesso se as credenciais forem válidas
            } else {
                console.log("Credenciais inválidas."); // Registra no console que as credenciais são inválidas
                return res.status(401).json({ message: "Credenciais inválidas." }); // Retorna uma resposta de erro se as credenciais forem inválidas
            }
        } catch (error) {
            console.error("Erro ao fazer login:", error.message); // Registra no console se ocorrer um erro durante o processo de login
            return res.status(400).json({ message: error.message }); // Retorna uma resposta de erro se ocorrer um erro durante o processo de login
        }
    }
}

const router = require('express').Router()
const usuarioController = require('../controller/usuarioController.js')
const produtoController = require('../controller/produtoController.js')
const dbController = require('../controller/dbController.js')

router.get('/getUser', usuarioController.getUsuario)
router.get('/Login', usuarioController.Login)
router.put('/usuarioAtualizar', usuarioController.updateUsuario)
router.delete('/deleteUser/:id', usuarioController.deleteUsuarioById);
router.post('/usuarioPost', usuarioController.postUsuario);

//Rotas dos produtos
router.get('/getProduto', produtoController.getProduto);
router.put('/produtoAtualizar', produtoController.updateProduto);
router.delete('/deleteProduto/:id', produtoController.deleteProdutoPorId);
router.post('/produto', produtoController.postProduto);

module.exports = router;